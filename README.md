# NodeJS Simple Translation Library

## Overview
The TransNode class is intended to provide a very basic translation facility for both browser and node.js use.  A single set of translation files named as ./public/translation.{lng}.json provides table lookup for symbols in templated DOM elements or strings.  Symbols delimited by '%' percent symbols are replaced with the locale's translation.  Functions for programmatic use are also available.

## Debian/Ubuntu example environment preparation 
The `setcap` command allows our user-land program to listen to the otherwise-restricted ports, including 80.  This example runs under supervisor so node will be refreshed with changes immediately without manual restarts.
```bash
sudo apt-get install nodejs npm libcap2-bin
sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``
sudo npm -g install supervisor
npm install
npm start
```

## Instantiation

### Browser (client-side)
Instantiate a new translator instance:
```javascript
var tran = new TransNode('en',function() {
  tran.t('hum');
  tran.translateDOM();
}
```

### Node.js (server-side)
Instantiate a new translator instance:
```javascript
// sourced from the same file as the browser
var tn = require(__dirname+'/../public/trans_node.js');
var tran = new tn.TransNode('en',function() {
  console.info(tran.t('hum'));
  console.info(tran.translateTemplate('avoid the %dog%'));
}
```

## Functions

### function t(symbol)
If a translation for the symbol exists in the loaded locale, it is returned.  If no matching symbol was found, the symbol itself is returned and an error is logged on the console.

### function translateTemplate(template)
Uses `t()` to translate all symbols in the `template`.

### function translateDOM()
Uses `translateTemplate()` to substitute translations for all percent-delimited symbols in all DOM elements having the attribute `transcontext="lang"`

### function loadLocale(locale,callback)
Load the translation file for the specified locale.  When done, call the callback function (no arguments).  The callback should be used in the browser to call `translateDOM()`, and in node.js to prepare any localized global strings.  This function is called by the constructor automatically, and therefore does not need to be called explicitly unless the locale must be changed on the fly.  This function does NOT update previously translated symbols -- that's a feature for the next version!

### function getLocale()
returns the current locale string, or false when the translator instance is invalid.

## Licence
This code is distributed under the MIT license.

The MIT License (MIT)

Copyright (c) 2016 Erik V.R. Wool.  

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
