// app root URIs

console.info(__dirname+'/../public/js/trans_node.js');
var tn = require(__dirname+'/../public/js/trans_node.js');
var express = require('express');
var tran = new tn.TransNode('fr',function(){
  console.log('checkin '+typeof tran.t);
  console.log('callback tran.t(hum):'+tran.t('test','hum'));
  var template = 'my %hum% is a %dog%';
  console.log('callback tran.template('+template+'):'+tran.translateTemplate('test',template));
});

// --- build root uri + authentication router ---
var router = express.Router();

// home page & variants
router.get('/',function(req, res) {
  console.log('test page, yo');
  res.render('test',{ title:'Test Page' });
});

module.exports = router;

// EOF

