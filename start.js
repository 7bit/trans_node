#!/usr/bin/env node

console.log('require...');
var http = require('http');
var path = require('path');
var express = require('express');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use('/', require('./routes/test'));
app.use(express.static(path.join(__dirname, 'public')));

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: err
  });
});

console.log('start...');
app.set('port', process.env.PORT || 80);
var server = http.createServer(app).listen(80);
console.log('started...');

module.exports = app;
