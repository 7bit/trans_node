// detect execution environment and load translation file
// if no locale specified, use 'en'
// when used in nodejs, fs MUST be loaded

// when an instance of this class is created, it lacks 
// functions dependent on having a loaded translation.
// these are attached directly to the new transnode object 
// after load is deemed successful.

// to-do
// - support more than just 2-letter language code locale, including fallbacks.
// - handle locale autodetection in node.js
// - add re-translation in configureLocale(). 
//   cache template version of DOM elements to be revised on locale change.

// invoke callback after system has loaded
function TransNode(locale,callback) {
  this.debug = false;

  // table object holds symbol->string information
  // false value indicates that a TransNode instance is in an invalid state
  this.table = false; 

  // environment contains string indicating either 
  // 'browser' - script is in a web client context
  // 'nodejs' - script is in a node.js context
  this.environment = false;
  this.loadEnvironment();

  // locale may be either 'll' or 'll-NN'
  // defaults to 'en'
  this.loadLocale(locale,callback);
} // end function TransNode()

TransNode.prototype.loadEnvironment = function() {
  this.environment = false;
  if (typeof module !== 'undefined' && module.exports) {
    this.environment = 'nodejs';
    this.node_fs = require('fs');
  } else if (typeof window !== 'undefined') {
    this.environment = 'browser';
  } else {
    throw "environment not detected";
  }
  if (this.debug) {
    console.info('TransNode detected environment['+this.environment+']');
  }
} // end function loadEnvironment()

// load specified locale translation file or throw an error
TransNode.prototype.loadLocale = function(locale,callback) {
  if (locale && locale.length > 2) {
    locale = locale.substring(0,2);
  }
  if (!locale || this.locale != locale || this.table == false) {
    this.detectLocale(locale);

    // reset instance state to avoid inconsistency
    this.table = false;
    if (this.hasOwnProperty('t') || this.hasOwnProperty('translateDOM') || this.hasOwnProperty('translateTemplate')) {
      delete this.t;
      delete this.translateDOM;
      delete this.translateTemplate;
      delete this.translateGlobalDOM;
    }

    // load translation file per environment
    if (this.environment == 'browser') {
      this.loadLocaleAjax(callback);
    } else if (this.environment == 'nodejs') {
      this.loadLocaleFile(callback);
    } else {
      throw 'TransNode.loadLocale failed for environment ['+this.environment+']';
    }
  } else {
    if (this.debug) {
      console.info('not reloading locale ['+this.locale+']');
    }
  }
} // end function TransNode.loadLocale()

TransNode.prototype.detectLocale = function(locale) {
  this.locale = 'en'; // default fallback
  if (locale) {
    this.locale = locale.substring(0,2);
  } else {
    if (this.environment == 'browser' && typeof window.navigator.language == 'string' && window.navigator.language.length > 1) {
      if (this.debug) {
        console.info('TransNode browser language: '+window.navigator.language);
      }
      this.locale = window.navigator.language.substring(0,2);
    } else if (this.environment == 'nodejs' && true) { /// check for existence
      /// we don't have req here -- see to-do
      ///console.info('TransNode nodejs language: '+req.headers["accept-language"]);
      ///this.locale = req.headers["accept-language"].substring(0,2);
    }
  }
  if (this.debug) {
    console.info('TransNode detected locale['+this.locale+']');
  }
} // end function TransNode.detectLocale()

// retrieve locale file from ajax call 
// meant for browser use
TransNode.prototype.loadLocaleAjax = function(callback) {
  var self = this;
  var url = 'locale/'+self.locale+'/translation.json'; 
  $.ajax({
    url: url,
    dataType: "json",
    success: function(data){
      self.table = data;
      self.configureLocale(self,callback);
    },
    error: function() {
      console.error('could not load translation file ['+url+'] from server');
      self.locale = 'en';
      url = '/locale/'+self.locale+'/translation.json';
      $.ajax({
        url: url,
        dataType: "json",
        success: function(data) {
          self.table = data;
          self.configureLocale(self,callback);
        },
        error: function() {
          console.error('could not load backup translation file from server');
          self.locale = false;
        }
      });
    }
  });
} // end function TransNode.loadLocaleAjax()

// retrieve locale file from local filesystem 
// meant for nodejs use
TransNode.prototype.loadLocaleFile = function(callback) {
  var self = this;
  var filepath = __dirname+'/../locale/'+self.locale+'/translation.json';
  if (this.debug) {
    console.info('TransNode loading file ['+filepath+']');
  }
  this.node_fs.readFile(filepath,function(err,data) {
    if (err) {
      self.locale = false;
      console.error('TransNode failed to load file ['+filepath+']');
    } else {
      self.table = JSON.parse(data);
      self.configureLocale(self,callback);
    }
  });
} // end function TransNode.loadLocaleFile()

// adds actionable functions to an instance 
// after it's loaded translation files
TransNode.prototype.configureLocale = function(instance,callback) {

  // compactly-named function to return localized string for symbol
  instance.t = function(group,symbol) {
    if (instance.table.hasOwnProperty(group) && instance.table[group].hasOwnProperty(symbol)) {
      return instance.table[group][symbol];
    } else {
      console.error('TransNode.t failed to find symbol ['+symbol+'] for locale['+instance.locale+'] + group['+group+']');
    }
    return symbol; // failsafe
  } // end function TransNode.t()

  // translate all %symbol% occurrences within a string
  instance.translateTemplate = function(group,template) {
    if (instance.table.hasOwnProperty(group)) {
      var keys = Object.keys(instance.table[group]);
      for (var k in keys) {
        template = template.replace('%'+keys[k]+'%',instance.t(group,keys[k]));
      }
    } else {
      console.error('TransNode.translateTemplate failed to find group['+group+'] for locale['+instance.locale+']');
    }
    return template;
  } // end function TransNode.translateTemplate()

  // translate all %symbol% occurrences for a group within DOM elements
  instance.translateDOM = function(group) {
    var targets = $('[transcontext='+group+']');
    targets.each(function(t) {
      $(this).html(instance.translateTemplate(group,$(this).html()));
    });
  } // end function TransNode.translateDOM()

  // translate all %symbol% occurrences in all groups within DOM elements
  instance.translateGlobalDOM = function() {
    var groups = Object.keys(instance.table);
    for (var g in groups) {
      instance.translateDOM(groups[g]);
    };
  } // end function TransNode.translateGlobalDOM()

  if (typeof callback != 'undefined') {
    callback(); // guaranteed to be executed AFTER function declarations above
  }

} // end function TransNode.configureLocale()

TransNode.prototype.getLocale = function() {
  return this.table ? this.locale : false;
} // end function TransNode.getLocale()

if (typeof module != 'undefined') {
  /// modify here to support req as a constructor argument
  module.exports = { TransNode:TransNode }; 
}

// EOF
